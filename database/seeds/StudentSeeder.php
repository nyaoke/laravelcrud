<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker::create();
        foreach (range(1,20) as $index){
        	DB::table('students')->insert([
        		'name'=>$faker->name,
        		'regno'=>$faker->numberBetween(1000,9999),
        		'country'=>$faker->country,
        		'phone'=>$faker->phoneNumber,
	        ]);
        }
    }
}
