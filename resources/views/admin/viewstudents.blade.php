@extends('layouts.app')

@section('content')
	<div class="container">
		<a href="{{route('addstudent')}}" class="btn btn-primary btn-sm">Add student</a>
		<div class="row justify-content-center">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header"> Students</div>

					<div class="card-body">
						@include('includes.messages')
 <table class="table table-bordered table-hover">
	 <thead>
		<th>#</th>
		<th>Name</th>
		<th>Reg no</th>
		<th>Country</th>
		<th>Phone</th>
		<th>Actions</th>
	 </thead>
	 <tbody>
	 @foreach($students as $key=>$student)
		 <tr>
			 <td>{{$key+1}}</td>
			 <td>{{$student->name}}</td>
			 <td>{{$student->regno}}</td>
			 <td>{{$student->country}}</td>
			 <td>{{$student->phone}}</td>
			 <td><a class="btn btn-info btm-sm" href="{{route('editstudent',$student->id)}}">Edit</a>
			 <a class="btn btn-danger btm-sm" href="{{route('remove',$student->id)}}">Delete</a> </td>
		 </tr>
		 @endforeach
	 </tbody>
 </table>

					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
