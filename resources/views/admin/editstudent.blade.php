@extends('layouts.app')

@section('content')
	<div class="container">
		<a href="{{route('students')}}" class="btn btn-primary btn-sm">Students</a>
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">Edit student</div>

					<div class="card-body">
						@include('includes.messages')
						<form method="post" action="{{route('updatestudent',$student->id)}}">
							@csrf
							<div class="form-group">
								<label for="email">Name:</label>
								<input type="text" class="form-control" name="name" value="{{$student->name}}" required>
							</div>
							<div class="form-group">
								<label for="pwd">Regno:</label>
								<input type="text" class="form-control" name="regno" value="{{$student->regno}}" required>
							</div>
							<div class="form-group">
								<label for="pwd">Place:</label>
								<input type="text" class="form-control" name="country" value="{{$student->country}}" required>
							</div>
							<div class="form-group">
								<label for="pwd">Place:</label>
								<input type="text" class="form-control" name="phone" value="{{$student->phone}}" required>
							</div>
							<div class="form-group form-check">
								<label class="form-check-label">

								</label>
							</div>
							<button type="submit" class="btn btn-primary btn-sm">Update</button>
						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
