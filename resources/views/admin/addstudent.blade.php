@extends('layouts.app')

@section('content')
	<div class="container">
		<a href="{{route('students')}}" class="btn btn-primary btn-sm">Students</a>
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">New student</div>

					<div class="card-body">
@include('includes.messages')
						<form method="post" action="{{route('poststudent')}}">
							@csrf
							<div class="form-group">
								<label for="email">Name:</label>
								<input type="text" class="form-control" name="name" required>
							</div>
							<div class="form-group">
								<label for="pwd">Regno:</label>
								<input type="text" class="form-control" name="regno" required>
							</div>
							<div class="form-group">
								<label for="pwd">Place:</label>
								<input type="text" class="form-control" name="country" required>
							</div>
							<div class="form-group">
								<label for="pwd">Phone:</label>
								<input type="text" class="form-control" name="phone" required>
							</div>
							<div class="form-group form-check">
								<label class="form-check-label">

								</label>
							</div>
							<button type="submit" class="btn btn-primary btn-sm">Register</button>
						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
