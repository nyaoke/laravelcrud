<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/student/add', 'StudentController@addstudent')->name('addstudent');
Route::post('/student/add', 'StudentController@poststudent')->name('poststudent');
Route::get('/students', 'StudentController@students')->name('students');
Route::get('/student/remove/{id}', 'StudentController@remove')->name('remove');
Route::get('/student/edit/{id}', 'StudentController@editstudent')->name('editstudent');
Route::post('/student/update/{id}', 'StudentController@updatestudent')->name('updatestudent');
