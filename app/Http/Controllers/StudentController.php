<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function addstudent(){
    	return view('admin.addstudent');
    }

    public  function poststudent(Request $request){
    	$student=Student::create($request->all());
    	return redirect()->back()->with('success','Student registered successfully');
    }

    public function students(){
    	$students=Student::all();
    	return view('admin.viewstudents',compact('students'));

    }

    public  function  remove($id){
    	$student=Student::find($id);
    	$student->delete();
	    return redirect()->back()->with('success','Student deleted successfully');
    }

    public  function editstudent($id){
    	$student=Student::find($id);
	    return view('admin.editstudent',compact('student'));
    }

    public  function updatestudent(Request $request,$id){
    	$student=Student::find($id);
    	$student->update($request->all());
	    return redirect()->back()->with('success','Student updated successfully');
    }
}
